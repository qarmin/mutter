### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update
apt install -y git
git clone https://github.com/GNOME/mutter.git
cd mutter

#Z#
apt build-dep -y mutter pipewire 
apt install -y sysprof libjack-jackd2-dev libbluetooth-dev libvulkan-dev git

git clone https://gitlab.freedesktop.org/pipewire/pipewire.git
cd pipewire

meson build/
ninja -C build/
ninja -C build/ install
cd ../
rm -rf pipewire

meson build 

ninja -C build

```
### QTCreator Includes
```
/usr/include/gtk-3.0
/usr/include/glib-2.0
/usr/include/mutter-6/clutter
/usr/include/pango-1.0/
/usr/include/atk-1.0/

```
